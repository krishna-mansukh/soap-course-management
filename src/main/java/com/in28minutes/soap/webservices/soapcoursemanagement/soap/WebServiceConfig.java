package com.in28minutes.soap.webservices.soapcoursemanagement.soap;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

//Enable spring web Services
@EnableWs
//Spring configuration
@Configuration
public class WebServiceConfig {

	//MessageDispatcherServlet
		// applicationContext
	// URL -> /ws/*    ---->> here we want to map MessageDispatcherServlet to mentioned url
	
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
		
		MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
		messageDispatcherServlet.setApplicationContext(context);
		messageDispatcherServlet.setTransformWsdlLocations(true);
		
		return new ServletRegistrationBean(messageDispatcherServlet, "/ws/*");
		
	}
	
	
	@Bean
	public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema coursesSchema) {
		
		DefaultWsdl11Definition definition=new DefaultWsdl11Definition();
		definition.setPortTypeName("CoursePort");
		definition.setTargetNamespace("http://in28minutes.com/courses");
		definition.setLocationUri("/ws");
		definition.setSchema(coursesSchema);
		
		
		return definition;
		
	}
	
	// /ws/courses.wsdl  by using schema defined in course-details.xsd
	// for above configuration we need to mentioned portType and NameSpace..etc
	@Bean
	public XsdSchema coursesSchema(){
		return new SimpleXsdSchema(new ClassPathResource("course-details.xsd"));
	}
	
	
}
